import logging
import random

logging.basicConfig(filename='client.log', level=logging.DEBUG)

HORIZONTAL = 1
VERTICAL = 0
SNAKE_HEAD = 0

FIELD_WIDTH = -1
FIELD_HEIGHT = -1

MATCH_COUNTER = 0


def step(snake_coordinates, food_coordinates, direction, field_size):
    global FIELD_WIDTH
    global FIELD_HEIGHT

    FIELD_WIDTH = field_size["width"]
    FIELD_HEIGHT = field_size["height"]

    # want to check if border reached and turn at 90 degrees
    dir = turn_if_border(snake_coordinates[0], direction)
    if dir != "not":
        return dir

    dir = turn_if_tail(snake_coordinates, direction)
    if dir != "not":
        return dir

    return turn_for_food(snake_coordinates, food_coordinates, direction)


def turn_for_food(snake_coordinates, food_coordinates, direction):
    snake_head_coordinates = snake_coordinates[0]
    global MATCH_COUNTER

    # if snake_head_coordinates[0] == food_coordinates[0]:
    #   if snake_head_coordinates[1] >= food_coordinates[1]:
    #        return "left"
    #    elif snake_head_coordinates[1] < food_coordinates[1]:
    #        return "right"

    if snake_head_coordinates[1] == food_coordinates[1]:
        MATCH_COUNTER += 1

    if snake_head_coordinates[1] == food_coordinates[1] and MATCH_COUNTER % 5 == 0:
        if snake_head_coordinates[0] >= food_coordinates[0]:
            return "up"
        elif snake_head_coordinates[0] < food_coordinates[0]:
            return "down"

    return direction


def turn_if_border(snake_coordinates, direction):
    if direction == "right":
        if snake_coordinates[1] == FIELD_WIDTH - 2:
            if snake_coordinates[0] > 1:
                return "down"
            else:
                return "down"


    elif direction == "left":
        if snake_coordinates[1] == 1:
            if snake_coordinates[0] > 1:
                return "up"
            else:
                return "up"

    elif direction == "down":
        if snake_coordinates[0] == FIELD_HEIGHT - 2:
            if snake_coordinates[1] > 1:
                return "left"
            else:
                return "left"

    elif direction == "up":
        if snake_coordinates[0] == 1:
            if snake_coordinates[1] > 1:
                return "right"
            else:
                return "right"

    return "not"


def turn_if_tail(snake_coordinates, direction):
    snake_head_coordinates = snake_coordinates[0]

    next_coordinates = [0, 0]

    if direction == "right":
        next_coordinates = [snake_head_coordinates[0], snake_head_coordinates[1] + 1]
    elif direction == "left":
        next_coordinates = [snake_head_coordinates[0], snake_head_coordinates[1] - 1]
    elif direction == "down":
        next_coordinates = [snake_head_coordinates[0] + 1, snake_head_coordinates[1]]
    elif direction == "up":
        next_coordinates = [snake_head_coordinates[0] - 1, snake_head_coordinates[1]]

    for val in snake_coordinates[:-1]:
        if val == next_coordinates:
            break
    else:
        return "not"

    if direction == "up" or direction == "down":
        direction = "left"
    elif direction == "left" or direction == "right":
        direction = "up"

    return direction